package anu.ericm.trickyandroid3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created by ericm on 16/03/17.
 */

public class MyView extends View implements View.OnTouchListener, Runnable {

    float tarX = 0.0f;
    float xt = 0.0f;
    float yt = 0.0f;
    int score = 0;
    boolean check = true;
    boolean movingRight = true;
    Handler timer;
    Bitmap myImage;


    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setOnTouchListener(this);
        myImage = BitmapFactory.decodeResource(getResources(),R.drawable.blob);
        timer = new Handler();
        timer.postDelayed(this,10);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (tarX == canvas.getWidth()) {
            movingRight = false;
        } else if (movingRight == false && tarX == 0){
            movingRight = true;
        }
        Paint p = new Paint();
        Paint p2 = new Paint();
        Paint p3 = new Paint();
        p3.setTextSize(200);
        p2.setColor(Color.RED);
        p.setColor(Color.BLUE);
        p.setStrokeWidth(10.0f);
        canvas.drawCircle(tarX, canvas.getHeight()/2.0f, 200.0f,p);
        canvas.drawCircle(xt, yt, 25.0f,p2);
        canvas.drawText(score + "", canvas.getWidth()/10.0f, canvas.getHeight()/10.0f,p3);
        if (check == true) {
            float val = distance(xt, yt, tarX, canvas.getHeight()/2.0f);
            if (val < 175.0f) {
                score++;
            }

            check = false;
        }
        //canvas.drawBitmap(myImage, xt, yt, p);
    }

    public float distance (float x1, float y1, float x2, float y2) {
        float result = 0.0f;

        float dx = x1 - x2;
        float dy = y1 - y2;
        result = (float) Math.sqrt(dx*dx + dy*dy);
        return result;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            xt = event.getX();
            yt = event.getY();
            this.invalidate();
            check = true;
        }
        return true;
    }

    @Override
    public void run() {
        if (movingRight) {
            tarX += 5.0f;
            this.invalidate();
            timer.postDelayed(this,10);
        } else {
            tarX -= 5.0f;
            this.invalidate();
            timer.postDelayed(this,10);
        }

    }
}
